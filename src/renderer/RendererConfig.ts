import {StageConfig} from "konva";
import {Engine} from "../physics/Engine";

export class RendererConfig  {
    public container: string;
    public engine: Engine;
}
