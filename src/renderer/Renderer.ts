import * as Konva from "konva";
import {Layer} from "konva";
import {Engine} from "../physics/Engine";
import {RendererConfig} from "./RendererConfig";
import {ArkanoidObject} from "../model/ArkanoidObject";

export class KonvaRenderer {

    public static create(config: RendererConfig): KonvaRenderer {

        return new KonvaRenderer(config);

    }

    private engine: Engine;
    private stage: Konva.Stage;
    private layer: Konva.Layer;

    private constructor(config: RendererConfig) {
        console.info('renderer options: ', config);
        this.engine = config.engine;
        this.stage = new Konva.Stage({
            container: 'canvas',   // id of container <div>
            width: config.engine.width,
            height: config.engine.height
        });

        this.engine.on('objectRemoved', (event: ArkanoidObject) => {
            const shapes = event.shapes();
            shapes.forEach((shape) => {
                shape.remove();
            });
        });
        this.engine.on('objectAdded', (event: ArkanoidObject[]) => {
            event.forEach((o) => {
                const shapes = o.shapes();
                shapes.forEach((shape) => {
                    this.layer.add(shape);
                });
            });
        });

        this.layer = new Konva.Layer();

        for (const o of this.engine.objects) {
            // console.info(o);
            for (const node of o.shapes()) {
                // console.info(node);
                this.layer.add(node);
            }
        }
        const background = new Konva.Rect({
            x: 0, y: 0,
            width: 800, height: 600,
            fillLinearGradientStartPoint: {x: 0, y: 100/ 2},
            fillLinearGradientEndPoint: { x : 0, y : 500},
            fillLinearGradientColorStops: [0, '#0182F3', 1, '#7DC2FE'],

        });

        this.layer.add(background);
        background.moveToBottom();
        this.stage.add(this.layer);
    }

    public getLayer(): Layer {
        return this.layer;
    }

    public render(frame: any): void {
        this.engine.objects.forEach((o) => {
           o.render(this.getLayer());
        });
    }
}