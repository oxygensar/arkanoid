import {Engine} from "../physics/Engine";
import {Block} from "../model/Block";
import {Ball} from "../model/Ball";
import {Wall} from "../model/Wall";
import {Vector2} from "../utils/Vector2";
import {Platform} from "../model/Platform";
import {BackboneEvents} from "../utils/BackboneEvents";
import {Capsule} from "../model/Capsule";
import {GameLevel} from "./GameLevel";

export class ArkanoidGame extends BackboneEvents {
    public blockWidth: number = 60;
    public blockHeight: number = 25;

    private blocksNo: number = 0;
    private currentScore: number = 0;

    private scheduled: any[] = [];

    private lives: number = 3;

    constructor(public engine: Engine, public gameConfig: any) {
        super();
    }
    
    public load(): void {

        const gameLevel = new GameLevel(this);
        gameLevel.load();

        const platform = this.createPlatform();
        const ball = this.createBall(200);

        this.engine.addObject(platform);
        this.engine.addObject(ball);

        this.engine.on('collision', (event: any) => {

            if (event.circle.label === 'ball' || event.circle.label === 'bullet') {
                switch (event.rectangle.label) {
                    case 'ground':
                        if (!this.gameConfig.debug) {
                            this.lives--;
                            this.updateScore();
                        }
                        if (this.lives <= 0) {
                            this.trigger('loss', this.currentScore);
                        }
                        break;

                    case 'block':
                        this.updateScore(this.currentScore + (event.rectangle.level + 1) * 10);
                        this.engine.removeObject(event.rectangle);

                        if (event.circle.label === 'bullet') {
                            this.engine.removeObject(event.circle);
                        }

                        if (Math.random() <= .1) {
                            const capsule =
                                new Capsule(
                                    event.rectangle.x + event.rectangle.height / 2,
                                    event.rectangle.y + event.rectangle.width / 2,
                                    'blueCapsule'
                                );
                            capsule.velocity = new Vector2(0, 50);
                            this.engine.addObject(capsule);
                        }
                        if (Math.random() <= .1) {
                            const capsule =
                                new Capsule(
                                    event.rectangle.x + event.rectangle.height / 2,
                                    event.rectangle.y + event.rectangle.width / 2,
                                    'roseCapsule'
                                );
                            capsule.velocity = new Vector2(0, 50);
                            this.engine.addObject(capsule);
                        }
                        if (Math.random() <= .1) {
                            const capsule =
                                new Capsule(
                                    event.rectangle.x + event.rectangle.height / 2,
                                    event.rectangle.y + event.rectangle.width / 2,
                                    'redCapsule'
                                );
                            capsule.velocity = new Vector2(0, 50);
                            this.engine.addObject(capsule);
                        }
                        if (Math.random() <= .1) {
                            const capsule =
                                new Capsule(
                                    event.rectangle.x + event.rectangle.height / 2,
                                    event.rectangle.y + event.rectangle.width / 2,
                                    'grayCapsule'
                                );
                            capsule.velocity = new Vector2(0, 50);
                            this.engine.addObject(capsule);
                        }
                        if (--this.blocksNo === 0) {
                            this.trigger('victory', this.currentScore);
                        }
                        break;
                    case 'wall':
                        if (event.circle.label === 'bullet') {
                            this.engine.removeObject(event.circle);
                        }

                    default:
                }
            } else if (event.circle.label === 'blueCapsule') {
                switch (event.rectangle.label) {
                    case 'ground':
                        this.engine.removeObject(event.circle);
                        break;
                    case 'platform':
                        if (!event.rectangle.mult) {
                            event.rectangle.x -= event.rectangle.width / 2;
                            event.rectangle.width *= 2;
                            event.rectangle.mult = true;

                            this.runLater(5, () => {
                                event.rectangle.width /= 2;
                                event.rectangle.x += event.rectangle.width / 2;
                                event.rectangle.mult = false;
                            });
                        }
                        this.engine.removeObject(event.circle);
                        break;

                }
            } else if (event.circle.label === 'roseCapsule') {
                switch (event.rectangle.label) {
                    case 'ground':
                        this.engine.removeObject(event.circle);
                        break;
                    case 'platform':
                        this.engine.removeObject(event.circle);
                        if (!ball.slowed) {
                            ball.velocity.scale(.5);
                            ball.slowed = true;

                            this.runLater(5, () => {
                                ball.velocity.scale(2);
                                ball.slowed = false;
                            });
                        }
                        break;
                }
            } else if (event.circle.label === 'redCapsule') {
                switch (event.rectangle.label) {
                    case 'ground':
                        this.engine.removeObject(event.circle);
                        break;
                    case 'platform':
                        this.engine.removeObject(event.circle);
                        if (!event.rectangle.lazers) {
                            event.rectangle.lazers = true;

                            this.runLater(15, () => {
                                event.rectangle.lazers = false;
                            });
                        }
                        break;
                }
            } else if (event.circle.label === 'grayCapsule') {
                switch (event.rectangle.label) {
                    case 'ground':
                        this.engine.removeObject(event.circle);
                        break;
                    case 'platform':
                        this.engine.removeObject(event.circle);
                        this.lives ++;
                        this.updateScore();
                        break;
                }
            }
        });
        this.engine.on('time', (timeDiff: number) => {

            if (this.scheduled.length > 0) {
                this.scheduled.forEach((task) => {
                    task.seconds -= timeDiff / 1000;
                    if (task.seconds <= 0) {
                        task.r();
                    }
                });
                this.scheduled = this.scheduled.filter((task) => task.seconds > 0);
            }
        });

            // keyboard paddle events
        $('body').on('keydown', (e) => {
            if (e.which === 37) { // left arrow key
                platform.velocity.x = -300;
            } else if (e.which === 39) { // right arrow key
                platform.velocity.x = 300;
            } else if (e.which === 32) {
                platform.fire();
            }
        });
        $('body').on('keyup', (e) => {
            if (e.which === 37) { // left arrow key
                platform.velocity.x = 0;
            } else if (e.which === 39) { // right arrow key
                platform.velocity.x = 0;
            } else if (e.which === 32) {
                platform.stopFire();
            }
        });

        $('.move-left').on("mousedown", (e) => {
            e = e || window.event;
            const button = e.which || e.button;
            if (e.button === 0) { // left click
                platform.velocity.x = -300;
            }
        });
        $('.move-left').on("mouseup", (e) => {
            e = e || window.event;
            const button = e.which || e.button;
            if (e.button === 0) { // left click
                platform.velocity.x = 0;
            }
        });
        $('.move-right').on("mousedown", (e) => {
            e = e || window.event;
            const button = e.which || e.button;
            if (e.button === 0) { // left click
                platform.velocity.x = 300;
            }
        });
        $('.move-right').on("mouseup", (e) => {
            e = e || window.event;
            const button = e.which || e.button;
            if (e.button === 0) { // left click
                platform.velocity.x = 0;
            }
        });

        $('.fire').on("mousedown", (e) => {
            e = e || window.event;
            const button = e.which || e.button;
            if (e.button === 0) { // left click
                platform.fire();
            }
        });
        $('.fire').on("mouseup", (e) => {
            e = e || window.event;
            const button = e.which || e.button;
            if (e.button === 0) { // left click
                platform.stopFire();
            }
        });
    }

    protected runLater(seconds: number, r: () => void) {
        this.scheduled.push({seconds, r});
    }

    public createWalls(): Wall[] {
        const wallHeight = 10;

        const    ground = new Wall(0, this.engine.height - wallHeight, this.engine.width, wallHeight);
        const   topWall = new Wall(0, 0, this.engine.width, wallHeight);
        const  leftWall = new Wall(0, 0, wallHeight, this.engine.height);
        const rightWall = new Wall(this.engine.width - wallHeight, 0, wallHeight, this.engine.height);

        ground   .label = 'ground';
        topWall  .label = 'wall';
        leftWall .label = 'wall';
        rightWall.label = 'wall';

        return [ground, leftWall, rightWall, topWall];
    }

    private createPlatform(): Platform {
        const platform = new Platform(this.engine, 400 - 50, 610 - 60 - 10, 100, 30);
        platform.velocity = new Vector2(0, 0);

        return platform;
    }

    private createBall(ballSpeed: number): Ball {
        const ball = new Ball(400, 610 - 60 - 10 - 30);
        ball.velocity = new Vector2(Math.floor(Math.random() * 21) + 20, -ballSpeed);
        ball.debug = this.gameConfig.debug;

        return ball;
    }

    public createBlock(left: number, top: number, blocks: Block[]) {
        const bh = this.blockHeight;
        const bw = this.blockWidth;

        for (let i = 0; i < 3; i++) {
            for (let q = 0; q < 8; q++) {

                this.blocksNo++;
                blocks.push(
                    new Block(left + (i * bw), top + (q * bh), bw, bh, (7 - q) % 4)
                );
            }
        }
    }

    private updateScore(newCurrentScore: number = this.currentScore): void {
        this.currentScore = newCurrentScore;
        this.trigger('score', { currentScore: this.currentScore, lives: this.lives });
    }

}
