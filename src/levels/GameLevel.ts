import {ArkanoidGame} from "./ArkanoidGame";
import {Block} from "../model/Block";

export class GameLevel {

    constructor(public game: ArkanoidGame) {
    }

    public load(): void {

        this.game.engine.addObject.apply(this.game.engine, this.game.createWalls());

        this.game.engine.addObject.apply(this.game.engine, this.createBlocks());

    }

    private createBlocks(): Block[] {
        const margin = 50;
        const left = (this.game.engine.width - 3 * ((3 * this.game.blockWidth) + margin)) / 2;
        const top = 50;

        const blocks: Block[] = [];

        for (let i = 0; i < 3; i++) {
            this.game.createBlock(left + (this.game.blockWidth * 3 + margin) * i, top, blocks);
        }
        return blocks;
    }

}
