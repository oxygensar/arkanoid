import {ArkanoidObject, ICircle, IRectangle} from "../model/ArkanoidObject";
import {BackboneEvents} from "../utils/BackboneEvents";
import {Vector2} from "../utils/Vector2";
import {quadtree} from "d3-quadtree";

export class Engine extends BackboneEvents {
    private mObjects: ArkanoidObject[] = [];
    private mWidth: number = 800;
    private mHeight: number = 600;

    constructor() {
        super();

    }

    public addObject(...objects: ArkanoidObject[]): void {
        this.mObjects = this.mObjects.concat(objects);

        this.trigger('objectAdded', objects);
    }
    public removeObject(object: ArkanoidObject) {
        const index = this.mObjects.indexOf(object, 0);
        if (index > -1) {
            this.mObjects.splice(index, 1);
            this.trigger('objectRemoved', object);
        }
    }

    public update(frame: any): void {
        this.trigger('time', frame.timeDiff);

        const smallObjects = quadtree<ArkanoidObject>()
            .x((o) => o.x)
            .y((o) => o.y)
            .extent([[-1, -1], [this.width + 1, this.height + 1]])
            .addAll(this.objects);

        const hugeObjects = this.objects.filter((o) => o.label === 'wall' || o.label === 'ground');

        for (const o of this.objects) {
            if (o.velocity && o.velocity.getLength() > 0) {

                o.x = o.x + o.velocity.x * (frame.timeDiff / 1000);
                o.y = o.y + o.velocity.y * (frame.timeDiff / 1000);

                o.x = Math.min(Math.max(0, o.x), this.width - this.getWidth(o));
                o.y = Math.min(Math.max(0, o.y), this.height - this.getHeight(o));

                if (isCircle(o)) {
                    smallObjects.visit(this.collide(o));

                    hugeObjects.forEach( (o2) => this.checkCollides(o, o2));

                }
            }
            o.update(frame);
        }
    }
    public get objects(): ArkanoidObject[] {
        return this.mObjects;
    }

    public get height(): any {
        return this.mHeight;
    }

    public set height(value: any) {
        this.mHeight = value;
    }
    public get width(): any {
        return this.mWidth;
    }

    public set width(value: any) {
        this.mWidth = value;
    }

    private collide(circle: any) {
        const r = circle.radius + 200;
        const nx1 = circle.x - r;
        const nx2 = circle.x + r;
        const ny1 = circle.y - r;
        const ny2 = circle.y + r;

        return (quad: any, x1: any, y1: any, x2: any, y2: any) => {
            if (quad.data && (quad.data !== circle)) {

                this.checkCollides(circle, quad.data);
            }
            return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
        };
    }

    private checkCollides(o: any, o2: ArkanoidObject) {
        if (!isCircle(o2)) {
            if (isRect(o2)) {
                if (this.collides(o2, o, false)) {

                    this.trigger('collision', {circle: o, rectangle: o2});

                    if (!o.ghost && !o2.ghost) {
                        this.bounceVector(o2, o);
                    }
                }
            }
        }
    }

    private collides(rect: IRectangle, circle: ICircle, collideInside: boolean): boolean {
        // compute a center-to-center vector
        const half = {x: rect.width / 2, y: rect.height / 2};
        const center = {
            x: circle.x - (rect.x + half.x),
            y: circle.y - (rect.y + half.y)
        };

        // check circle position inside the rectangle quadrant
        const side = {
            x: Math.abs(center.x) - half.x,
            y: Math.abs(center.y) - half.y
        };

        if (side.x > circle.radius || side.y > circle.radius) { // outside
            return false;
        }
        if (side.x < -circle.radius && side.y < -circle.radius) { // inside
            return collideInside;
        }
        if (side.x < 0 || side.y < 0) { // intersects side or corner
            return true;
        }

        // circle is near the corner
        return side.x * side.x + side.y * side.y < circle.radius * circle.radius;
    }

    /*
     * based on https://stackoverflow.com/questions/573084/how-to-calculate-bounce-angle/573206#573206
     */
    private bounceVector(rect: IRectangle, circle: ICircle): void {
        const NearestX = Math.max(rect.x, Math.min(circle.x, rect.x + rect.width));
        const NearestY = Math.max(rect.y, Math.min(circle.y, rect.y + rect.height));
        const dist = new Vector2(circle.x - NearestX, circle.y - NearestY);

        const norm = dist.normalize();

        const velocity = circle.velocity.copy();

        const u = norm.copy().scale(Vector2.dot(velocity, norm)).negate();
        const w = velocity.subtract(u);

        const bounceVelocity = w.subtract(u);

        const penetrationDepth = circle.radius - dist.getLength();
        const penetrationVector = bounceVelocity.copy().normalize().scale(penetrationDepth);
        const newPos = new Vector2(circle.x, circle.y).subtract(penetrationVector);

        circle.velocity = bounceVelocity;
        circle.x = newPos.x;
        circle.y = newPos.y;

    }
    private getWidth(o: ArkanoidObject): number {
        return isRect(o) ? o.width : isCircle(o) ? o.radius : 0;
    }

    private getHeight(o: ArkanoidObject) {
        return isRect(o) ? o.height : isCircle(o) ? o.radius : 0;
    }
}
function isCircle(o: any): o is ICircle {
    return o.radius !== undefined;
}
function isRect(o: any): o is IRectangle {
    return o.width !== undefined;
}
