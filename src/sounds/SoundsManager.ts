import {Engine} from "../physics/Engine";

declare var Wad: any; // Magic


export class SoundsManager {

    private hitSound: any;

    public constructor(public engine: Engine) {
        this.hitSound = new Wad({source: 'hit.wav', volume: 0.2});

        this.engine.on('collision', (event: any) => {
            if (!event.circle.ghost && !event.rectangle.ghost) {
                this.hitSound.play();
            }
        });
    }

}