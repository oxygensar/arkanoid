import * as Konva from "konva";
import {ArkanoidObject, ICircle} from "./ArkanoidObject";

export class Ball extends ArkanoidObject implements ICircle {

    public radius: number = 10;

    public slowed: boolean = false;

    private cicle?: Konva.Circle;

    constructor(x: number, y: number) {
        super();
        this.x = x;
        this.y = y;
        this.label = 'ball';
    }

    public shapes(): Konva.Node[] {
        if (!this.cicle) {
            this.cicle = new Konva.Circle({
                x: this.x,
                y: this.y,
                radius: this.radius,
                fill: 'blue',
                stroke: 'black',
                strokeWidth: 1,
                draggable: this.debug
            });

            // add cursor styling
            this.cicle.on('mouseover', () => {
                document.body.style.cursor = 'pointer';
            });
            this.cicle.on('mouseout', () => {
                document.body.style.cursor = 'default';
            });
            this.cicle.on('dragend', () => {
                if (this.cicle) {
                    this.x = this.cicle.x();
                    this.y = this.cicle.y();
                }
            });

        }

        const result: Konva.Node[] = [this.cicle];

        return result.concat(super.shapes());
    }

    public render(layer: Konva.Layer): void {
        super.render(layer);

        if (this.cicle) {
            this.cicle.x(this.x);
            this.cicle.y(this.y);
        }

    }

}
