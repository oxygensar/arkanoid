import * as Konva from "konva";
import {ArkanoidObject, ICircle} from "./ArkanoidObject";

export class Bullet extends ArkanoidObject implements ICircle {

    public radius: number = 5;

    private cicle?: Konva.Circle;

    constructor(x: number, y: number) {
        super();
        this.x = x;
        this.y = y;
        this.label = 'bullet';
        this.ghost = true;
    }

    public shapes(): Konva.Node[] {
        if (!this.cicle) {
            this.cicle = new Konva.Circle({
                x: this.x,
                y: this.y,
                radius: this.radius,
                fill: 'gray',
                stroke: 'black',
                strokeWidth: 1
            });
        }

        return [this.cicle];
    }

    public render(layer: Konva.Layer): void {
        super.render(layer);

        if (this.cicle) {
            this.cicle.x(this.x);
            this.cicle.y(this.y);
            // console.info('position: ', this.x, this.y);
        }
    }

}
