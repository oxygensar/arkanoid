import * as Konva from "konva";
import {Vector2} from "../utils/Vector2";

export abstract class ArkanoidObject {
    public label: string = 'object';
    public speed: number;
    public velocity: Vector2;
    public x: number;
    public y: number;

    public ghost: boolean = false;

    public debug: boolean = true;

    private arrow: Konva.Arrow;

    public shapes(): Konva.Node[] {
        if (this.debug) {
            if (!this.arrow) {
                this.arrow = new Konva.Arrow({
                    // x: this.x,
                    // y: this.y,
                    points: [this.x, this.y, this.x + this.velocity.x, this.y + this.velocity.y],
                    pointerLength: 15,
                    pointerWidth : 15,
                    fill: 'black',
                    stroke: 'black',
                    strokeWidth: 1
                });
            }
            return [this.arrow];
        }
        return [];
    }
    public update(frame: any): void {
        // nop
    }
    public render(layer: Konva.Layer): void {
        if (this.debug) {
            if (this.arrow) {
                this.arrow.points([this.x, this.y, this.x + this.velocity.x, this.y + this.velocity.y]);
            }

        }

    }

}

export interface ICircle {

    x: number;
    y: number;

    radius: number;

    velocity: Vector2;
}
export interface IRectangle {

    x: number;
    y: number;

    width: number;
    height: number;

}
