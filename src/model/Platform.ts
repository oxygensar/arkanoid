import * as Konva from "konva";
import {RectObject} from "./RectObject";
import {Engine} from "../physics/Engine";
import {Bullet} from "./Bullet";
import {Vector2} from "../utils/Vector2";

export class Platform extends RectObject {

    public lazers: boolean = false;

    public lazerTurrets: Konva.Circle[] = [];

    private totalTimeDiff: number;
    private firing: boolean = false;

    constructor(public engine: Engine, x: number, y: number, width: number, height: number) {
        super(x, y, width, height);
        this.label = 'platform';
    }

    public update(frame: any) {
        if (this.lazers && this.firing) {
            this.totalTimeDiff += frame.timeDiff / 1000;
            let fireCount = this.totalTimeDiff * 2;
            // this.totalTimeDiff = this.totalTimeDiff % 2;

            while (fireCount-- > .5) {
                this.totalTimeDiff -= 0.5;
                if (this.rect) {
                    this.engine.addObject(
                        this.createBullet(
                            this.rect.x() + 10,
                            this.rect.y() - this.rect.height() / 2
                        )
                    );
                    this.engine.addObject(
                        this.createBullet(
                            this.rect.x() + this.rect.width() - 10,
                            this.rect.y() - this.rect.height() / 2
                        )
                    );
                }
            }
        }
    }

    public shapes(): Konva.Node[] {
        if (!this.rect) {
            this.rect = new Konva.Rect({
                x: this.x,
                y: this.y,
                width: this.width,
                height: this.height,
                // fill: 'blue',
                stroke: 'black',
                strokeWidth: 1,
                cornerRadius: 70,
                fillLinearGradientStartPoint: {x: 0, y: -this.height / 2},
                fillLinearGradientEndPoint: {x: 0, y: this.height / 2},
                fillLinearGradientColorStops: [0, 'silver', 1, 'black'],
                shadowOffsetX: 10,
                shadowOffsetY: 15,
                shadowBlur: 40,
                opacity: 0.5
            });
        }

        return [this.rect];
    }

    public render(layer: Konva.Layer): void {
        super.render(layer);

        if (this.lazers) {
            if (this.rect) {
                if (this.lazerTurrets.length === 0) {
                    this.lazerTurrets[0] = new Konva.Circle({
                        x: this.rect.x() + 10,
                        y: this.rect.y() - this.rect.height() / 2,
                        radius: 10,
                        fill: 'red',
                        stroke: 'black',
                        strokeWidth: 1
                    });
                    this.lazerTurrets[1] = new Konva.Circle({
                        x: this.rect.x() + this.rect.width() - 10,
                        y: this.rect.y() - this.rect.height() / 2,
                        radius: 10,
                        fill: 'red',
                        stroke: 'black',
                        strokeWidth: 1
                    });

                    layer.add(this.lazerTurrets[0]);
                    layer.add(this.lazerTurrets[1]);
                }

                this.lazerTurrets[0].x(this.rect.x() + 10);
                this.lazerTurrets[1].x(this.rect.x() + this.rect.width() - 10);
            }

        } else {
            if (this.lazerTurrets.length !== 0) {
                this.lazerTurrets.forEach((t) => t.remove());
                this.lazerTurrets = [];
            }
        }
    }

    public fire(): void {
        if (this.lazers && !this.firing) {
            this.totalTimeDiff = 1;
            this.firing = true;
        }
    }
    public stopFire(): void {
        this.firing = false;
    }

    private createBullet(x: number, y: number): Bullet {
        const bullet = new Bullet(x, y);
        bullet.velocity = new Vector2(0, -300);
        return bullet;
    }

}
