import * as Konva from "konva";
import {ArkanoidObject, IRectangle} from "./ArkanoidObject";

export class RectObject extends ArkanoidObject implements IRectangle {

    public height: number;
    public width: number;

    protected rect?: Konva.Rect;

    constructor(x: number, y: number, width: number, height: number) {
        super();
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
    }

    public shapes(): Konva.Node[] {
        if (!this.rect) {
            this.rect = new Konva.Rect({
                x: this.x,
                y: this.y,
                width: this.width,
                height: this.height,
                fill: 'gray',
                stroke: 'black',
                strokeWidth: 1
            });
        }

        return [this.rect];
    }

    public render(layer: Konva.Layer): void {
        super.render(layer);

        if (this.rect) {
            this.rect.x(this.x);
            this.rect.y(this.y);
            this.rect.width(this.width);
            // console.info('position: ', this.x, this.y);
        }
    }
}
