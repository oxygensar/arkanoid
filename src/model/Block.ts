import {RectObject} from "./RectObject";
import * as Konva from "konva";

export class Block extends RectObject {

    constructor(x: number, y: number, width: number, height: number, public level: number) {
        super(x, y, width, height);
        this.label = 'block';
    }

    public shapes(): Konva.Node[] {
        if (!this.rect) {
            this.rect = new Konva.Rect({
                x: this.x,
                y: this.y,
                width: this.width,
                height: this.height,
                // fill: 'blue',
                stroke: 'black',
                strokeWidth: 1,
                cornerRadius: 70,
                fillLinearGradientStartPoint: {x: 0, y: -this.height / 3},
                fillLinearGradientEndPoint: { x : 0, y : this.height / 2},
                fillLinearGradientColorStops: [0, 'white', 1, this.level2color(this.level)],
            });
        }

        return [this.rect];
    }

    public render(): void {

        // if (this.rect) {
        //     this.state = (this.state + 1) % 80;
        //
        //     this.rect.fillLinearGradientStartPointX(this.state+10);
        //     this.rect.fillLinearGradientEndPointX(this.state + 20);
        // }
        // fillLinearGradientEndPoint: { x : 0, y : this.height / 2},

    }

    private level2color(level: number): string {
        switch (level) {
            case 0:
                return 'red';
            case 1:
                return 'blue';
            case 2:
                return 'green';
            default:
                return '#B70DF4';
        }
    }
}
