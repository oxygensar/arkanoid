import {RectObject} from "./RectObject";

export class Wall extends RectObject {

    constructor(x: number, y: number, width: number, height: number) {
        super(x, y, width, height);
    }

}
