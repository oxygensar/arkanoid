import * as Konva from "konva";
import {ArkanoidObject, ICircle} from "./ArkanoidObject";

export class Capsule extends ArkanoidObject implements ICircle {

    public radius: number = 15;

    private cicle?: Konva.Circle;

    constructor(x: number, y: number, label: string) {
        super();
        this.x = x;
        this.y = y;
        this.label = label;
        this.ghost = true;
    }

    public shapes(): Konva.Node[] {
        if (!this.cicle) {
            this.cicle = new Konva.Circle({
                x: this.x,
                y: this.y,
                radius: this.radius,
                fill: this.label2color(),
                stroke: 'black',
                strokeWidth: 1
            });
        }

        return [this.cicle];
    }

    public render(layer: Konva.Layer): void {
        super.render(layer);

        if (this.cicle) {
            this.cicle.x(this.x);
            this.cicle.y(this.y);
            // console.info('position: ', this.x, this.y);
        }
    }

    private label2color(): string {
        switch (this.label) {
            case 'blueCapsule':
                return '#9ec3ff';
            case 'roseCapsule':
                return '#ff9ede';
            case 'redCapsule':
                return 'red';
            default:
                return 'gray';
        }
    }
}
