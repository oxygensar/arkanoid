import * as $ from "jquery";
import {ArkanoidLauncher} from './ArkanoidLauncher';

// Set jquery '$' variable as a global
(window as any).$ = $;

/*  
    Entry point of your app is the static method 'Main()' in the 'Arkanoid' class
    (Like in C#)
*/ 
$(() => {
    ArkanoidLauncher.Main();
});
