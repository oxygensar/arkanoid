import {Engine} from "./physics/Engine";
import {KonvaRenderer} from "./renderer/Renderer";
import {SoundsManager} from "./sounds/SoundsManager";
import * as Konva from "konva";
import {ArkanoidGame} from "./levels/ArkanoidGame";

export class ArkanoidLauncher {
    public static Main(): void {

        const config = {
            debug: false,

            ballSpeed: 200,

            pause: false
        };

        let launcher = new ArkanoidLauncher(config);

        launcher.startGame();

        $('.restart-trigger').on('click', (e) => {

            launcher.stop();
            $('canvas').remove();
            launcher = new ArkanoidLauncher(config);
            launcher.startGame();
        });

        $('.pause-trigger').on('click', (e) => {
            launcher.pause();
        });

        $('.update').get(0).style.display = (config.debug ? 'block' : 'none');
        $('.update').on('click', (e) => {
            launcher.anim.stop();
            launcher.engine.update({timeDiff: 1000 / 60});
            launcher.render.render({timeDiff: 1000 / 60});
            launcher.render.getLayer().drawScene();
        });

    }

    private engine: Engine;
    private render: KonvaRenderer;
    private anim: Konva.Animation;
    private soundsManager: SoundsManager;

    private gameOverPanel: JQuery;

    private $currentScore: JQuery;
    private $highScore: JQuery;
    private $lives: JQuery;

    private highScore: number;

    constructor(public gameConfig: any) {
        this.$currentScore = $('.current-score span');
        this.$highScore = $('.high-score span');
        this.$lives = $('.lives span');

        this.gameOverPanel = $('.modal');
        this.gameOverPanel.get(0).style.display = "none";

        this.highScore = 0; // TODO load from somewhere
    }
    public startGame() {

        this.engine = new Engine();
        this.soundsManager = new SoundsManager(this.engine);

        const game = new ArkanoidGame(this.engine, this.gameConfig);
        game.on('victory', (score: number) => {
            this.processVictory();
        });
        game.on('loss', (score: number) => {
            this.processLoss();
        });
        game.on('score', (newScore: number) => {
            this.updateScore(newScore);
        });
        game.load();

        // create a renderer
        const container: HTMLElement = $('.container').get(0);

        this.render = KonvaRenderer.create({
            container: 'container',
            engine: this.engine,
        });

        this.anim = new Konva.Animation((frame: any) => {
            // let time = frame.time,
            //     timeDiff = frame.timeDiff,
            //     frameRate = frame.frameRate;

            this.engine.update(frame);
            this.render.render(frame);
            // update stuff
        }, this.render.getLayer());

        if (!this.gameConfig.pause) {
            this.anim.start();
        }
    }

    public stop(): void {
        this.anim.stop();
    }

    public pause(): void {
        if (this.anim.isRunning()) {
            this.anim.stop();
        } else {
            this.anim.start();
        }
    }

    private processVictory() {
        this.gameOverPanel.get(0).style.display = "block";
        $(".modal .won").get(0).style.display = "block";
        $(".modal .lost").get(0).style.display = "none";
        this.anim.stop();
    }

    private processLoss(): void {
        this.gameOverPanel.get(0).style.display = "block";
        $(".modal .won").get(0).style.display = "none";
        $(".modal .lost").get(0).style.display = "block";
        this.anim.stop();
    }

    private updateScore(score: any): void {
        this.$currentScore.text(score.currentScore);

        this.highScore = Math.max(score.currentScore, this.highScore);
        this.$highScore.text(this.highScore);
        this.$lives.text(score.lives);
    }
}
